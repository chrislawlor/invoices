import posixpath
from fabric.api import env, run, cd, prefix, require
from contextlib import contextmanager as _contextmanager


def production():
    env.hosts = ['invoices.christopherlawlor.com']
    env.directory = "/home/clawlor/webapps/invoices/src/"
    env.requirements = "production.txt"

    venv_root = "/home/clawlor/.virtualenvs/invoices/"
    env.venv_activate = posixpath.join(venv_root, 'bin', 'activate')
    env.env_vars = posixpath.join(venv_root, 'bin', 'postactivate')

    env.restart_appserver_script = '/home/clawlor/webapps/invoices/apache2/bin/restart'
    env.user = 'clawlor'


TARGETS = ['production']


@_contextmanager
def env_vars():
    require('env_vars', provided_by=TARGETS)
    with prefix("source %(env_vars)s" % env):
        yield


@_contextmanager
def virtualenv():
    require('directory', 'venv_activate', provided_by=TARGETS)
    with cd(env.directory):
        with prefix("source %(venv_activate)s" % env):
            with env_vars():
                yield


def checkout_head():
    """
    Checkout the specified revision, or HEAD
    """
    require('directory', provided_by=TARGETS)
    with cd(env.directory):
        run("git pull")


def restart_appserver():
    """
    Restart the application server.
    """
    require('restart_appserver_script', provided_by=TARGETS)
    with virtualenv():
        run(env.restart_appserver_script)


def collectstatic():
    with virtualenv():
        run("python apps/manage.py collectstatic --noinput")


def install_requirements():
    require('requirements', provided_by=TARGETS)
    with virtualenv():
        run("pip install -r requirements/%(requirements)s" % env)


def run_migrations():
    with virtualenv():
        run("python apps/manage.py migrate")


def deploy():
    checkout_head()
    install_requirements()
    run_migrations()
    collectstatic()
    restart_appserver()


def quick_deploy():
    """
    Just do a fresh checkout and restart the appserver.
    """
    checkout_head()
    restart_appserver()
