from django.test import TestCase
from django.core.urlresolvers import reverse

from invoices.models import HarvestInvoice
from profiles.models import InvoicesUser


class LoginRedirectTests(TestCase):
    def setUp(self):
        self.invoice = HarvestInvoice.objects.create(
            status='open',
            amount='100.00',
            number='1',
            subject='test invoice',
        )

    def test_basic_urls(self):
        for url in ['/']:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)

    def test_logged_in_urls(self):
        user = InvoicesUser.objects.create_user(
            "Test", "User", "testuser@example.com", password='password'
        )
        self.client.login(email=user.email, password='password')

        urls = [
            reverse('invoices-invoice-detail', kwargs={'pk': self.invoice.pk}),
            reverse('invoices-invoice-list')
        ]

        for url in urls:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)

    # unauthenticated link to invoice detail should redirect to login.
    def test_invoice_detail_redirect(self):
        response = self.client.get(self.invoice.get_absolute_url())
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/profiles/login/?next=/invoices/1')
