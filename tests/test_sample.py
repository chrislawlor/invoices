# Place your tests in this directory
# Be sure to prefix module names with "test_"

from django.test import TestCase


class SimpleTest(TestCase):
    def test_load_pages(self):
        for page in ['/', '/profiles/login/']:
            response = self.client.get(page)
            self.assertEqual(200, response.status_code)
