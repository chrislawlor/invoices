from django.http import Http404
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth import login, authenticate
from django.views.generic.base import RedirectView
from django.views.generic.edit import FormView
from django.views.generic.list import ListView

from braces.views import (
    LoginRequiredMixin, StaffuserRequiredMixin
)

from .models import InvoicesUser
from .forms import NewClientForm, SetPasswordForm


class StaffLoginRequired(LoginRequiredMixin, StaffuserRequiredMixin):
    pass


class UserEmailCodeMixin(object):
    def user():
        def fget(self):
            if not hasattr(self, '_user'):
                code = self.kwargs.get('code')
                try:
                    user = InvoicesUser.objects.get_from_encoded_email(code)
                    self._user = user
                except InvoicesUser.DoesNotExist:
                    raise Http404
            return self._user

        def fset(self, value):
            self._user = value

        def fdel(self):
            del self._user

        return locals()
    user = property(**user())


class RegistrationView(UserEmailCodeMixin, RedirectView):
    permanent = False

    def get_redirect_url(self, **kwargs):
        code = kwargs.get('code')
        if not self.user.is_active:
            # First time sign on. Activate and redirect to change password view
            return reverse('invoices-password-set', kwargs={'code': code})
        return reverse('login')


class PasswordSet(UserEmailCodeMixin, FormView):
    form_class = SetPasswordForm
    template_name = "invoices/password_set_form.html"
    success_url = reverse_lazy('invoices-invoice-list')

    def get_form(self, form_class):
        return form_class(self.user, **self.get_form_kwargs())

    def form_valid(self, form):
        form.save()
        user = authenticate(email=self.user.email,
                            password=form.cleaned_data['password1'])
        login(self.request, user)
        return super(PasswordSet, self).form_valid(form)

    def get_context_data(self, *args, **kwargs):
        context = super(PasswordSet, self).get_context_data(*args, **kwargs)
        context['new_user'] = self.user
        return context


class ContactList(StaffLoginRequired, ListView):
    queryset = InvoicesUser.objects.filter(is_admin=False, is_staff=False)
    context_object_name = "contacts"
    template_name = "invoices/contact_list.html"


class ContactCreate(StaffLoginRequired, FormView):
    template_name = "invoices/new_contact.html"
    form_class = NewClientForm
    success_url = reverse_lazy('invoices-contact-list')

    def form_valid(self, form):
        form.save()
        return super(ContactCreate, self).form_valid(form)
