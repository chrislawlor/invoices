from django.conf.urls import patterns, url, include

from .views import RegistrationView, ContactList, ContactCreate, PasswordSet


urlpatterns = patterns("",
    url(
        r'^invite/(?P<code>[A-Z0-9]+)/?$',
        RegistrationView.as_view(),
        name='invoices-invite'
    ),
    url(
        r'^set-password/(?P<code>[A-Z0-9]+)/?$',
        PasswordSet.as_view(),
        name='invoices-password-set'
    ),
    url(
        r'^contacts/$',
        ContactList.as_view(),
        name='invoices-contact-list'
    ),
    url(
        r'^contacts/new$',
        ContactCreate.as_view(),
        name='invoices-contact-create',
    ),
    url(r'', include('django.contrib.auth.urls')),
)
