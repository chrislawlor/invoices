from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import InvoicesUser


class InvoicesUserCreationForm(forms.ModelForm):
    """
    Form for creating new users.
    """
    password1 = forms.CharField(label="Password", widget=forms.PasswordInput)
    password2 = forms.CharField(label="Password, again",
                                widget=forms.PasswordInput)

    class Meta:
        model = InvoicesUser
        fields = ('first_name', 'last_name', 'email')

    def clean_password2(self):
        # Confirm password entries match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            msg = "Passwords don't match"
            raise forms.ValidationError(msg)
        return password2

    def save(self, commit=True):
        # save the user
        user = super(InvoicesUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user


class InvoicesUserChangeForm(forms.ModelForm):
    """
    A form for updating users.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = InvoicesUser

    def clean_password(self):
        # only return the initial value
        return self.initial['password']


class InvoicesUserAdmin(UserAdmin):
    add_form = InvoicesUserCreationForm
    form = InvoicesUserChangeForm

    list_display = ('last_name', 'first_name', 'email', 'get_invite_url')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('first_name', 'last_name', 'email')
    ordering = ('last_name', 'first_name')
    filter_horizontal = ('groups', 'user_permissions')
    fieldsets = (
        (None, {"fields": ('first_name', 'last_name', 'email', 'password')}),
        ("Harvest Info", {'fields': ('harvest_id', 'harvest_client_id',)}),
        ("Permissions", {"fields": ('is_active', 'is_staff', 'is_superuser',
                                    'groups', 'user_permissions')}),
        ("Important Dates", {"fields": ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'email', 'password1',
                       'password2')
        }),
    )

admin.site.register(InvoicesUser, InvoicesUserAdmin)
