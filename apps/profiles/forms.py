from django import forms

from .models import InvoicesUser


class NewClientForm(forms.ModelForm):
    class Meta:
        model = InvoicesUser
        fields = ('first_name', 'last_name', 'email', 'harvest_id',
                  'harvest_client_id')

    def save(self, **kwargs):
        user = InvoicesUser.objects.create_contact(**self.cleaned_data)
        return user


class SetPasswordForm(forms.ModelForm):
    user_id = forms.IntegerField(widget=forms.HiddenInput)
    password1 = forms.CharField(label="Password", widget=forms.PasswordInput)
    password2 = forms.CharField(label="Password, again",
                                widget=forms.PasswordInput)

    class Meta:
        model = InvoicesUser
        fields = ('user_id', 'password1', 'password2')

    def __init__(self, user, **kwargs):
        initial = kwargs.get('initial', {})
        initial['user_id'] = user.id
        kwargs['initial'] = initial
        super(SetPasswordForm, self).__init__(**kwargs)

    def clean_password2(self):
        # Confirm password entries match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            msg = "Passwords don't match"
            raise forms.ValidationError(msg)
        return password2

    def save(self, commit=True):
        # save the user
        user = InvoicesUser.objects.get(id=self.cleaned_data['user_id'])
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.is_active = True
            user.save()
        return user
