import base64

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.encoding import python_2_unicode_compatible


class InvoicesUserManager(BaseUserManager):
    def create_user(self, first_name, last_name, email, password=None,
                    harvest_id=None, harvest_client_id=None, is_active=True):
        """
        Creates and saves a User with the given email.
        """
        if not email:
            msg = "Users must have a valid email address."
            raise ValueError(msg)

        user = self.model(
            first_name=first_name,
            last_name=last_name,
            email=InvoicesUserManager.normalize_email(email),
            is_active=is_active
        )
        if harvest_id:
            user.harvest_id = harvest_id
        if harvest_client_id:
            user.harvest_client_id = harvest_client_id
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, first_name, last_name, email, password):
        """
        Creates and saves a superuser with the given email.
        """
        user = self.create_user(first_name, last_name, email, password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def create_contact(self, first_name, last_name, email, harvest_id,
                       harvest_client_id, password=None):
        user = self.create_user(first_name, last_name, email,
                                harvest_id=harvest_id, password=password,
                                harvest_client_id=harvest_client_id,
                                is_active=False)
        return user

    def get_from_encoded_email(self, encoded_email):
        email = base64.b16decode(encoded_email)
        return super(InvoicesUserManager, self).get(email=email)


class InvoicesUser(AbstractBaseUser, PermissionsMixin):
    """
    User model with no username.
    """
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField("email address", max_length=255, unique=True,
                              db_index=True)
    title = models.CharField(max_length=100, default="", blank=True)
    harvest_id = models.IntegerField(blank=True, null=True)
    harvest_client_id = models.IntegerField(blank=True, null=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name"]

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    objects = InvoicesUserManager()

    def get_full_name(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

    @python_2_unicode_compatible
    def __str__(self):
        return self.get_short_name

    def get_encoded_email(self):
        return base64.b16encode(self.email)

    def get_invite_url(self):
        code = self.get_encoded_email()
        url = reverse("invoices-invite", kwargs={'code': code})
        # TODO: Determine HTTP / HTTPS
        return "https://{0}{1}".format(settings.DOMAIN, url)
