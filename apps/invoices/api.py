from django.conf import settings

import requests


class HarvestAPIError(Exception):
    pass


def make_api_request(endpoint):
    headers = {
        'content-type': 'application/json',
        'accept': 'application/json',
    }
    auth = (settings.HARVEST_USERNAME, settings.HARVEST_PASSWORD)
    return requests.get(endpoint, headers=headers, auth=auth)


def get_harvest_invoice_response(invoice_id):
    url = "https://{0}/invoices/{1}".format(settings.HARVEST_SUBDOMAIN,
                                            invoice_id)
    return make_api_request(url)


def get_contacts_for_client(client_id):
    url = "https://{0}/clients/{1}/contacts".format(settings.HARVEST_SUBDOMAIN,
                                                    client_id)
    return make_api_request(url)
