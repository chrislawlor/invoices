from django import forms

from decimal import Decimal

from .models import HarvestInvoice
from .api import get_harvest_invoice_response


class HarvestImportError(Exception):
    """
    Raised when trying to import a harvest invoice,
    but there is no user that matches the client_id.
    """
    pass


class HarvestInvoiceLoader(forms.ModelForm):
    # When created, modified are added, format is:
    # 2013-07-12T21:39:03Z
    class Meta:
        model = HarvestInvoice
        fields = ('status', 'amount', 'number', 'subject', 'harvest_id',
                  'harvest_client_id', 'harvest_client_key', 'due')


class HarvestImportForm(forms.Form):
    invoice_id = forms.IntegerField()

    def clean(self):
        cleaned_data = super(HarvestImportForm, self).clean()
        invoice_id = cleaned_data['invoice_id']
        response = get_harvest_invoice_response(invoice_id)
        if response.status_code == 404:
            raise forms.ValidationError("Invoice Not Found")

        if response.status_code != 200:
            msg = "Harvest API returned a response code {0}".format(response.status_code)
            raise forms.ValidationError(msg)
        json = response.json()
        self.harvest_data = json.get('invoice')
        return cleaned_data

    def _get_harvest_loader_form_data(self):
        harvest_data = self.harvest_data

        return {
            'status': harvest_data['state'],
            'amount': Decimal(harvest_data['due_amount']),
            'number': harvest_data['number'],
            'subject': harvest_data['subject'],
            'harvest_id': harvest_data['id'],
            'harvest_client_id': harvest_data['client_id'],
            'harvest_client_key': harvest_data['client_key'],
            'due': harvest_data['due_at'],
            #'created': harvest_data['created_at'],
            #'modified': harvest_data['updated_at']
        }

    def save(self):
        form = HarvestInvoiceLoader(self._get_harvest_loader_form_data())
        if form.is_valid():
            invoice = form.save()
            return invoice
        else:
            raise HarvestImportError(form.errors)
