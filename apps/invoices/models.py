from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.core.urlresolvers import reverse
from django.conf import settings

from model_utils import Choices
from model_utils.models import StatusModel

from profiles.models import InvoicesUser


class HarvestInvoice(StatusModel):
    STATUS = Choices('open', 'draft', 'partial', 'paid', 'closed')
    contact = models.ForeignKey(InvoicesUser, blank=True, null=True)

    amount = models.DecimalField(decimal_places=2, max_digits=8)
    number = models.CharField(max_length=20)
    subject = models.CharField(max_length=200)

    harvest_id = models.IntegerField(blank=True, null=True)
    harvest_client_id = models.IntegerField(blank=True, null=True)
    harvest_client_key = models.CharField(max_length=45, default="",
                                          blank=True)
    due = models.DateField(blank=True, null=True)

    #created = models.DateTimeField()
    #modified = models.DateTimeField()

    class Meta:
        ordering = ("-due", "number")

    @python_2_unicode_compatible
    def __str__(self):
        return "{0} ({1})".format(self.number, self.status)

    def get_absolute_url(self):
        return reverse("invoices-invoice-detail", kwargs={'pk': self.pk})

    def get_harvest_public_url(self):
        if self.harvest_client_key:
            return "https://{0}/client/invoices/{1}".format(settings.HARVEST_SUBDOMAIN,
                                                            self.harvest_client_key)
        return None

    def allow_payment(self):
        return self.status in ['open', 'partial']

    def get_payment_url(self):
        return reverse('invoices-process-payment', kwargs={'pk': self.pk})

    def get_stripe_amount(self):
        return int(self.amount * 100)


class HarvestInvoiceLineItem(models.Model):
    invoice = models.ForeignKey(HarvestInvoice, related_name='line_items')
    kind = models.CharField(max_length=30)
    description = models.TextField()
    quantity = models.IntegerField()
    unit_price = models.DecimalField(decimal_places=2, max_digits=8)
    amount = models.DecimalField(decimal_places=2, max_digits=8)
    order = models.PositiveIntegerField()

    @python_2_unicode_compatible
    def __str__(self):
        return self.description
