from django.conf.urls import patterns, url

from .views import (
    InvoiceDetail, ImportHarvestInvoice, process_payment, InvoiceList
)


urlpatterns = patterns("",
    url(
        r'^(?P<pk>\d+)/?$',
        InvoiceDetail.as_view(),
        name='invoices-invoice-detail'
    ),
    url(
        r'^import$',
        ImportHarvestInvoice.as_view(),
        name='invoices-invoice-import'
    ),
    url(
        r'^(?P<pk>\d+)/process-payment$',
        process_payment,
        name='invoices-process-payment'
    ),
    url(
        r'^$',
        InvoiceList.as_view(),
        name='invoices-invoice-list',
    ),
)
