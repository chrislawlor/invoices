from django.contrib import admin

from .models import HarvestInvoice, HarvestInvoiceLineItem


class HarvestInvoiceAdmin(admin.ModelAdmin):
    pass


admin.site.register(HarvestInvoice, HarvestInvoiceAdmin)
