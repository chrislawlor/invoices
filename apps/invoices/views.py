from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.http import (
    HttpResponse, HttpResponseRedirect, Http404
)
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.contrib import messages
from django.utils.safestring import mark_safe

from braces.views import (
    LoginRequiredMixin, StaffuserRequiredMixin
)
import stripe

from .forms import HarvestImportForm, HarvestImportError
from .models import HarvestInvoice


stripe.api_key = settings.STRIPE_SECRET_KEY


class StaffLoginRequired(LoginRequiredMixin, StaffuserRequiredMixin):
    pass


class InvoiceList(LoginRequiredMixin, ListView):
    model = HarvestInvoice
    context_object_name = "invoices"
    template_name = "invoices/invoice_list.html"

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return HarvestInvoice.objects.all()
        if not user.harvest_client_id:
            return HarvestInvoice.objects.none()
        return HarvestInvoice.objects.filter(harvest_client_id=user.harvest_client_id)


class InvoiceDetail(LoginRequiredMixin, DetailView):
    model = HarvestInvoice
    context_object_name = 'invoice'
    template_name = 'invoices/invoice_detail.html'

    def get_object(self, queryset=None):
        invoice = super(InvoiceDetail, self).get_object(queryset=queryset)
        user = self.request.user
        if not user.is_staff and invoice.harvest_client_id != user.harvest_client_id:
            raise Http404
        return invoice


class ImportHarvestInvoice(StaffLoginRequired, FormView):

    form_class = HarvestImportForm
    template_name = "invoices/import_invoice.html"

    def form_valid(self, form):
        try:
            invoice = form.save()
        except HarvestImportError, e:
            return HttpResponse(e)
        return HttpResponseRedirect(invoice.get_absolute_url())


def process_payment(request, pk):
    invoice = get_object_or_404(HarvestInvoice, pk=pk)
    token = request.POST.get('stripe_token')
    if token is None:
        messages.error("Form processesing error")
        return HttpResponseRedirect(invoice.get_absolute_url())
    description = "Christopher Lawlor: Invoice {0}".format(invoice.number)

    try:
        charge = stripe.Charge.create(
            amount=invoice.get_stripe_amount(),
            currency="usd",
            card=token,
            description=description
        )
    except stripe.CardError:
        messages.error(request, "Your card has been declined")
        return HttpResponseRedirect(invoice.get_absolute_url())

    except stripe.AuthenticationError:
        messages.error(request, "There was an error connecting with the payment processor")
        return HttpResponseRedirect(invoice.get_absolute_url())

    if charge.paid:
        invoice.status = 'paid'
        invoice.save()
        msg = mark_safe("<strong>Thank You!</strong> Your payment has processed successfully.")
        messages.success(request, msg)
        return HttpResponseRedirect(invoice.get_absolute_url())
