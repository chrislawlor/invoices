from .base import *


RAVEN_CONFIG = {
    'dsn': get_env_var("INVOICES_RAVEN_DSN")
}

ALLOWED_HOSTS = ['invoices.christopherlawlor.com', ]

STATIC_ROOT = "/home/clawlor/webapps/invoices_assets/static/"
MEDIA_ROOT = "/home/clawlor/webapps/invoices_assets/media/"

STATIC_URL = "/assets/static/"
MEDIA_URL = "/assets/media/"
