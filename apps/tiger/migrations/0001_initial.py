# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Card'
        db.create_table(u'tiger_card', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('stripe_id', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('fingerprint', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('last4', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('exp_month', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('exp_year', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('address_line1', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('address_line2', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('address_state', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('address_zip', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('address_city', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('address_country', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='credit_cards', to=orm['profiles.InvoicesUser'])),
            ('deleted', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('is_default', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'tiger', ['Card'])

        # Adding model 'Invoice'
        db.create_table(u'tiger_invoice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('amount', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('submitted', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('charge_id', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('charge_attempts', self.gf('django.db.models.fields.TextField')()),
            ('card', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tiger.Card'])),
        ))
        db.send_create_signal(u'tiger', ['Invoice'])


    def backwards(self, orm):
        # Deleting model 'Card'
        db.delete_table(u'tiger_card')

        # Deleting model 'Invoice'
        db.delete_table(u'tiger_invoice')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'profiles.invoicesuser': {
            'Meta': {'object_name': 'InvoicesUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'harvest_client_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'harvest_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'tiger.card': {
            'Meta': {'object_name': 'Card'},
            'address_city': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'address_country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'address_line1': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'address_line2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'address_state': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'address_zip': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'deleted': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'exp_month': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'exp_year': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'fingerprint': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last4': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'stripe_id': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'credit_cards'", 'to': u"orm['profiles.InvoicesUser']"})
        },
        u'tiger.invoice': {
            'Meta': {'object_name': 'Invoice'},
            'amount': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tiger.Card']"}),
            'charge_attempts': ('django.db.models.fields.TextField', [], {}),
            'charge_id': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'submitted': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['tiger']