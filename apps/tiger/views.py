from django.http import HttpResponse, Http404, HttpResponseRedirect,\
    HttpResponseForbidden, HttpResponseBadRequest
from django.conf import settings

import stripe

from signals import stripe_event
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required

try:
    import simplejson as json
except ImportError:
    import json


from .models import Card
from .forms import CreditCardForm


STRIPE_DEBUG = getattr(settings, 'STRIPE_DEBUG', True)


def receive_webhook(request):
    if request.method != "POST":
        raise Http404
    data = json.loads(request.POST)
    # Confirm the event via stripe API
    event_id = data.get('id')
    if not event_id:
        return HttpResponseBadRequest()
    event = stripe.Event.retrieve(event_id)

    # both live and test webhooks are sent to production applications
    if not STRIPE_DEBUG and not event.livemode:
        # it's a test and we're in production, ignore.
        return HttpResponse()
    stripe_event.send(sender=stripe.Event, event=event)
    return HttpResponse()


def get_card_form_data(card):
    customer_info = card.get_stripe_customer()

    return {'card_number': card.last4,
            'card_type': card.type,
            'expiration_date': str(card.exp_month) + ' ' + str(card.exp_year),
            'stripe_token': card.fingerprint,
            'city': card.address_city,
            'billing_address': card.address_line1,
            'billing_address': card.address_line2,
            'billing_address': card.address_line2,
            'card_name': ''}


@login_required
def delete_card(request, card_id):
    card = get_object_or_404(Card, pk=card_id)
    if card.user != request.user:
        return HttpResponseForbidden('Not allowed')

    card.delete()
    return HttpResponseRedirect('/credit_cards/manage')


@login_required
def make_default_card(request, card_id):
    card = get_object_or_404(Card, pk=card_id)

    if card.user != request.user:
        return HttpResponseForbidden('Not allowed')

    Card.objects.filter(user=request.user).exclude(id=card_id).update(is_default=False)
    card.is_default = True
    card.save()

    return HttpResponseRedirect('/credit_cards/manage/')


@login_required
def edit_card(request, template='credit_card/add_card.html'):
    ctx = {}
    card_dict = {}
    results = {'success': False}

    if request.method == 'POST':
        form = CreditCardForm(request.POST, initial=card_dict)
        if form.is_valid():
            results.update({'success': True})
            try:
                card = Card.objects.create(request.user,
                                           form.cleaned_data['stripe_token'])
            except stripe.CardError, ex:
                # the token SHOULD be valid, but we can get here if we're
                # using a non test card when stripe is in test mode.
                results['success'] = False
                results['errors'] = {'card_type': ex.message}
                return HttpResponse(json.dumps(results),
                                    mimetype='application/json')
            # update credit card billing info
            card.address_line1 = form.cleaned_data.get('billing_address')
            card.address_line2 = form.cleaned_data.get('billing_address_2')
            card.address_state = form.cleaned_data.get('state')
            card.address_zip = form.cleaned_data.get('zip')
            card.city = form.cleaned_data.get('city')
            card.save()

            # if card is the only card, make it the default
            if Card.objects.filter(user=request.user).count() == 1:
                card.is_default = True
                card.save()

            if request.is_ajax():
                return HttpResponse(json.dumps(results),
                                    mimetype='application/json')

        else:
            errors = form.errors_as_json(strip_tags=True)
            results.update(errors)
            if request.is_ajax():
                return HttpResponse(json.dumps(results),
                                    mimetype='application/json')
    else:
        form = CreditCardForm(initial=card_dict)

    ctx.update({'form': form})

    return render(request, template, ctx)


@login_required
def manage_cards(request, template='credit_card/cards.html'):
    user_cards = Card.objects.get_query_set().filter(user=request.user)
    return render(request, template, {'user_cards': user_cards})
